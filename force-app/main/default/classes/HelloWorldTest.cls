@isTest
private class HelloWorldTest {


    @TestSetup static void makeData() {
        //prepare your initial data here ;)
    }
    @isTest static void test_IsActive() {
        helloWorld helloWorld = new helloWorld();
        Boolean isActive = helloWorld.isActive();
        System.assertEquals(true, isActive);
    }

     @isTest static void test_Greeting() {
         helloWorld helloWorld = new helloWorld();
        String testName = 'Esteban Lopez Betancourt';
        String greetingResultMsg = helloWorld.sayHello(testName);
        System.assertEquals('Hello my dear: Esteban Lopez Betancourt', greetingResultMsg);
     }
}
